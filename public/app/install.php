<?php

/**
 * install.php
 *
 */

/*
 * 1. public/app/configuration/default_config.ini
 */
try {
	if (file_exists ( './configuration/default_config.ini' )) {
	} else {
		throw new Exception ( 'File default_config.ini is missing.' );
	}
} catch ( Exception $e ) {
	
	$exception = get_class ( $e );
	
	header ( "Content-type: text/html" );
	var_dump ( $e );
	exit ();
}
